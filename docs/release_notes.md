# Release Notes

## 1.0.4

### Bug fix

- Exported filenames were using the original filenames, rather than the BIDSified filename.

## 1.0.3

### Docs

- Add release notes

### Testing  

- Expand the coverage of test_integration_bids_workflow to include re-import, and
re-curate steps to test ReproIn export methods (and ReproIn import methods)
- Test complex logic methods

### ENH

- Upload json's to the project level, if present. This change is aimed at including
task and sequence parameters.
- Add a semi-hidden overwrite toggle for upload_bids
- Allow BIDS sidecar information to be uploaded and BIDSified
- Introduce complex logic for rules (combining $or/$and/$not/etc)
