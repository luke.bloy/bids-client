# bids-client

FROM python:3.9-slim

LABEL maintainer="Flywheel <support@flywheel.io>"

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install -y --no-install-recommends nodejs npm bash jq && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# https://www.npmjs.com/package/bids-validator
RUN npm install -g bids-validator@1.8.4

ENV FLYWHEEL="/var/flywheel/code"
WORKDIR ${FLYWHEEL}

# Dev install. git for pip editable install.
# hadolint ignore=DL3008
RUN apt-get update && apt-get install -y --no-install-recommends git && \
    pip install --no-cache-dir "poetry==1.1.2" && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Installing main dependencies
COPY pyproject.toml poetry.lock $FLYWHEEL/
RUN poetry install --no-dev

RUN rm -rf /var/cache/apk/*

# Installing the current project (most likely to change, above layer can be cached)
# Note: poetry requires a README.md to install the current project
COPY README.md $FLYWHEEL/
COPY flywheel_bids $FLYWHEEL/flywheel_bids
RUN poetry install --no-dev
