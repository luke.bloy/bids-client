from unittest.mock import MagicMock

import pytest


@pytest.fixture
def mock_context(mocker):
    mocker.patch("flywheel_gear_toolkit.GearToolkitContext")
    gtk = MagicMock(autospec=True)
    return gtk
