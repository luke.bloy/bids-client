"""Unit test for zip_htmls"""

from os import chdir
from pathlib import Path
from unittest.mock import MagicMock, patch
from zipfile import ZipFile

import pytest

from flywheel_bids.results import zip_htmls

FWV0 = Path.cwd()


def test_zip_it_zip_it_good(tmpdir):
    """Test for zip_it_zip_it_good.

    Test zipping up a file results in a *.html.zip file with the expected file
    structure"""
    output_dir = tmpdir / "output"
    destination_id = "1234567890"
    name = "my_results.html"
    # zip_it_zip_it_good expects a file called "index.html" in the current folder
    chdir(tmpdir)
    Path("index.html").touch()

    output_dir.mkdir()
    zip_htmls.zip_it_zip_it_good(output_dir, destination_id, name)
    expected_zip_filename = output_dir / f"{name[:-5]}_{destination_id}.html.zip"
    print(f"Expected_zip: {expected_zip_filename}")
    assert Path(expected_zip_filename).exists()
    with ZipFile(expected_zip_filename, "r") as zip_object:
        assert zip_object.namelist() == ["index.html"]
    chdir(FWV0)


# Test 2 use cases:
# - html files in path: True/None
@pytest.mark.parametrize("html_files_in_path", [True, False])
@patch("flywheel_bids.results.zip_htmls.zip_it_zip_it_good")
def test_zip_htmls(mock_zip_it, html_files_in_path, tmpdir, caplog):
    """Test for zip_htmls."""
    output_dir = tmpdir / "output"
    destination_id = "1234567890"

    # create path where html files are to be found (if any):
    input_dir = tmpdir / "input"
    input_dir.mkdir()
    if html_files_in_path:
        # create (empty) html files:
        html_files = [input_dir / f"file{i}.html" for i in range(4)]
        # index.html is handled in a special way, so make sure we cover it
        html_files.append(input_dir / "index.html")
        for f in html_files:
            Path(f).touch()

    zip_htmls.zip_it_zip_it_good = MagicMock()

    zip_htmls.zip_htmls(output_dir, destination_id, input_dir)

    if html_files_in_path:
        assert zip_htmls.zip_it_zip_it_good.call_count == len(html_files)
        # make sure that the original index.html is still there (it has not been
        # overwritten and moved by zip_it_zip_it_good:
        assert (input_dir / "index.html").exists()
    else:
        assert "No *.html files" in caplog.text


def test_zip_htmls_path_doesnt_exist(tmpdir, caplog):
    """Test for zip_htmls when the target path does not exist."""

    zip_htmls.zip_htmls("foo", "dummy", tmpdir / "foo")
    assert "ERROR" in caplog.text
    assert "Path NOT found" in caplog.text
