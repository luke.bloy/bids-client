"""Test for when a json file in FW does not contain BIDS info

To make sure the specific acquisition (and files) are not deleted from the instance,
 we ship it in the assets folder"""

import json
import logging
import os
from pathlib import Path
from unittest import TestCase

import test_integration_bids_workflow

assets_path = Path(__file__).parent / "assets"
bids_dataset_dir = assets_path / "json_no_bids"

API_KEY = os.environ.get("FW_KEY")
GROUP_ID = "bids-client"
GROUP_LABEL = "BIDS Integration Testing"
PROJECT_LABEL = "bids-client-export"

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


def check_for_fw_key(user_json):
    """Check for FW's API key in $HOME/.config/flywheel/user.json.

    Check that there is a $HOME/.config/flywheel/user.json file, and that it
    contains a "key" entry (for FW's API). If not found, the test using this
    fixture is skipped.
    """
    if not user_json.exists():
        TestCase.skipTest("", f"{str(user_json)} file not found.")

    # Check API key is present:
    with open(user_json, "r", encoding="utf8") as f:
        j = json.load(f)
    if "key" not in j or not j["key"]:
        TestCase.skipTest("", f"No API key available in {str(user_json)}")


def test_export_json_no_bids(tmpdir):
    """Check that export_bids even when a json doesn't have BIDS metadata."""
    from _pytest.monkeypatch import MonkeyPatch

    MonkeyPatch().setattr(
        "flywheel_bids.supporting_files.utils.confirmation_prompt", lambda x: "no"
    )
    # check for API key; if not found, it skips this test:
    check_for_fw_key(Path.home() / ".config/flywheel/user.json")

    log.info("Uploading bids project to instance.")
    is_imported = test_integration_bids_workflow.run_flywheel_bids_import(
        api_key=API_KEY,
        group_id=GROUP_ID,
        group_label=GROUP_LABEL,
        project_label=PROJECT_LABEL,
        bids_dataset_dir=bids_dataset_dir,
    )

    if is_imported:
        log.info("Exporting bids.")
        is_exported = test_integration_bids_workflow.run_flywheel_bids_export(
            api_key=API_KEY,
            group_id=GROUP_ID,
            project_label=PROJECT_LABEL,
            output_bids_dataset_dir=tmpdir,
        )

    assert is_exported
