import os

import flywheel
import pytest

import flywheel_bids.upload_bids as upload_bids


@pytest.fixture(scope="module")
def client():
    key = os.getenv("FLYWHEEL_SDK_API_KEY")
    return flywheel.Client(key)


@pytest.fixture(scope="module")
def project(client):
    group = client.groups.find_first("_id=test")
    if group is None:
        group_id = client.add_group({"_id": "test", "label": "Test Group"})
    else:
        group_id = group["_id"]

    if group:
        project = group.projects.find_first("label=BIDS Test")
    else:
        project = None
    if project is None:
        project_id = client.add_project({"label": "BIDS Test", "group": group_id})
    else:
        project_id = project["_id"]

    yield project_id
    client.delete_project(project_id)


@pytest.fixture(scope="module")
def subject(project, client):
    subject = client.subjects.find_first(f"parents.project={project},label=sub-01")
    if subject is None:
        subject_id = client.add_subject({"project": project, "label": "sub-01"})
    else:
        subject_id = subject.id

    yield subject_id
    client.delete_subject(subject_id)


@pytest.fixture(scope="module")
def session(project, subject, client):
    session = client.sessions.find_first(f"parents.subject={subject},label=ses-01")
    if session is None:
        session_id = client.add_session(
            {"project": project, "subject": subject, "label": "ses-01"}
        )
    else:
        session_id = session.id

    yield session_id
    client.delete_session(session_id)


@pytest.fixture(scope="module")
def acquisition(session, client):
    acquisition = client.get_session(session).acquisitions.find_first(f"label=anat")
    if acquisition is None:
        acquisition_id = client.add_acquisition({"session": session, "label": "anat"})
    else:
        acquisition_id = acquisition.id

    yield acquisition_id
    client.delete_acquisition(acquisition_id)


@pytest.fixture(scope="module")
def test_file():
    with open("test.txt", "w") as fd:
        fd.write("Hello, world")

    yield "test.txt"
    os.remove("test.txt")


@pytest.fixture(scope="module")
def test_file_2():
    with open("test-2.txt", "w") as fd:
        fd.write("Hello, world")

    yield "test-2.txt"
    os.remove("test-2.txt")


@pytest.mark.integration
@pytest.mark.skip
def test_upload_project_file_should_return_file_info_of_uploaded_file(
    project, client, test_file, test_file_2
):
    context = {"project": {"id": project}}

    file_info = upload_bids.upload_project_file(client, context, test_file, test_file)
    file_info_2 = upload_bids.upload_project_file(
        client, context, test_file_2, test_file_2
    )

    assert file_info_2["name"] == test_file_2


@pytest.mark.integration
@pytest.mark.skip
def test_upload_subject_file_should_return_file_info_of_uploaded_file(
    project, subject, client, test_file, test_file_2
):
    context = {"project": {"id": project}, "subject": {"id": subject}}

    file_info = upload_bids.upload_subject_file(client, context, test_file, test_file)
    file_info_2 = upload_bids.upload_project_file(
        client, context, test_file_2, test_file_2
    )

    assert file_info_2["name"] == test_file_2


@pytest.mark.integration
@pytest.mark.skip
def test_upload_session_file_should_return_file_info_of_uploaded_file(
    project, subject, session, client, test_file, test_file_2
):
    context = {
        "project": {"id": project},
        "subject": {"id": subject},
        "session": {"id": session},
    }

    file_info = upload_bids.upload_session_file(client, context, test_file, test_file)
    file_info_2 = upload_bids.upload_session_file(
        client, context, test_file_2, test_file_2
    )

    assert file_info_2["name"] == test_file_2


@pytest.mark.integration
@pytest.mark.skip
def test_upload_acquisition_file_should_return_file_info_of_uploaded_file(
    project, subject, session, acquisition, client, test_file, test_file_2
):
    context = {
        "project": {"id": project},
        "subject": {"id": subject},
        "session": {"id": session},
        "acquisition": {"id": acquisition},
    }

    file_info = upload_bids.upload_project_file(client, context, test_file, test_file)
    file_info_2 = upload_bids.upload_project_file(
        client, context, test_file_2, test_file_2
    )

    assert file_info_2["name"] == test_file_2
