import os
import shutil
import unittest

import flywheel

from flywheel_bids.supporting_files import utils


class UtilsTestCases(unittest.TestCase):
    def setUp(self):
        # Define testdir
        self.testdir = "testdir"

    def tearDown(self):
        # Cleanup 'testdir', if present
        if os.path.exists(self.testdir):
            shutil.rmtree(self.testdir)

    def test_get_extension_nii(self):
        """Get extension if .nii"""
        fname = "T1w.nii"
        ext = utils.get_extension(fname)
        self.assertEqual(".nii", ext)

    def test_get_extension_niigz(self):
        """Get extension if .nii.gz"""
        fname = "T1w.nii.gz"
        ext = utils.get_extension(fname)
        self.assertEqual(".nii.gz", ext)

    def test_get_extension_tsv(self):
        """Get extension if .tsv"""
        fname = "T1w.tsv"
        ext = utils.get_extension(fname)
        self.assertEqual(".tsv", ext)

    def test_get_extension_none(self):
        """Assert function returns None if no extension present"""
        fname = "sub-01_T1w"
        ext = utils.get_extension(fname)
        self.assertIsNone(ext)

    def test_get_extension_fix1(self):
        """ """
        fname = "1.2.3.4.5.6.nii.gz"
        ext = utils.get_extension(fname)
        self.assertEqual(".nii.gz", ext)

    def test_get_extension_fix2(self):
        """ """
        fname = "1.2.3.4.5.json"
        ext = utils.get_extension(fname)
        self.assertEqual(".json", ext)

    def test_get_extension_fix3(self):
        """ """
        fname = "test.tar.gz"
        ext = utils.get_extension(fname)
        self.assertEqual(".tar.gz", ext)

    def test_get_extension_fix4(self):
        """ """
        fname = "test.tsv"
        ext = utils.get_extension(fname)
        self.assertEqual(".tsv", ext)

    def test_get_extension_fix5(self):
        """ """
        fname = "T1w.nii"
        ext = utils.get_extension(fname)
        self.assertEqual(".nii", ext)

    def test_get_extension_fix6(self):
        """ """
        fname = "{T1w.info}kajsdfk.nii.gz"
        ext = utils.get_extension(fname)
        self.assertEqual(".nii.gz", ext)

    def test_validate_bids_checks_real_paths_fails(self):
        found_validator = False
        for val_path in ["/usr/bin/bids-validator", "/usr/local/bin/bids-validator"]:
            if os.path.isfile(val_path):
                found_validator = True
        if not found_validator:
            unittest.skip("bids-validator not found")
        else:
            with self.assertLogs(level="DEBUG") as cm:
                utils.validate_bids("baddir")
                for ii, lin in enumerate(cm.output):
                    print(ii, lin)
                assert "bids-validator version" in cm.output[0]
                assert "baddir does not exist" in cm.output[3]

    def test_validate_bids_passes(self):
        with self.assertLogs(level="DEBUG") as cm:
            utils.BIDS_VALIDATOR_PATHS = ["/not/going/to/find/me", "/bin/echo"]
            utils.validate_bids("hey there")
            assert "Validating BIDS directory" in cm.output[0]
            assert "hey there" in cm.output[2]

    def test_validate_bids_fails(self):
        with self.assertLogs(level="DEBUG") as cm:
            utils.BIDS_VALIDATOR_PATHS = ["/bin/ls"]
            utils.validate_bids("nowhere")
            assert "Validating BIDS directory" in cm.output[0]
            assert "returncode: " in cm.output[1]

    def test_validate_bids_not_found(self):
        with self.assertLogs(level="DEBUG") as cm:
            utils.BIDS_VALIDATOR_PATHS = ["/bin/nope", "/nyet"]
            utils.validate_bids("nothing to see here")
            for lin in cm.output:
                print(lin)
            assert "not be found in ['/bin/nope', '/nyet']" in cm.output[0]

    @unittest.skip("Integration test")
    def test_validate_project_label_invalidproject(self):
        """Get project that does not exist. Assert function returns None.
        NOTE: you must be logged in "fw login ..." for this to work
        """
        client = flywheel.Client()
        label = "doesnotexistdoesnotexistdoesnotexist89479587349"
        with self.assertRaises(ValueError):
            utils.validate_project_label(client, label)

    @unittest.skip("Integration test")
    def test_validate_project_label_validproject(self):
        """Get project that DOES exist but there are more than one.
        NOTE: you must be logged in "fw login ..." for this to work
        """
        client = flywheel.Client()
        label = "Levitas_Tutorial"
        with self.assertRaises(ValueError):
            utils.validate_project_label(client, label)

    @unittest.skip("Integration test")
    def test_validate_project_group_validproject(self):
        """Get project that DOES exist. Assert function returns the project.
        NOTE: you must be logged in "fw login ..." for this to work
        """
        client = flywheel.Client()
        label = "Levitas_Tutorial"
        project_id = utils.validate_project_label(
            client, label, group_id="bids-tutorial"
        )
        project_id_expected = "60a55f2b43c676768f8526cf"
        self.assertEqual(project_id, project_id_expected)


if __name__ == "__main__":

    unittest.main()
