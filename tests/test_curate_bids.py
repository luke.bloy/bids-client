import os
import shutil
import unittest

from flywheel_bids import curate_bids
from flywheel_bids.supporting_files import project_tree
from flywheel_bids.supporting_files.templates import BIDS_V1_TEMPLATE, DEFAULT_TEMPLATE


class BidsCurateTestCases(unittest.TestCase):
    def setUp(self):
        # Define testdir
        self.testdir = "testdir"

    def tearDown(self):
        # Cleanup testdir, if present
        if os.path.exists(self.testdir):
            shutil.rmtree(self.testdir)

    def test_ValidateMetaInfo_Default_Valid(self):
        meta_info = {"info": {"BIDS": {}}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_ValidateMetaInfo_EmptyPath_Valid(self):
        meta_info = {"info": {"BIDS": {"Path": "", "extra": "test"}}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_ValidateMetaInfo_EmptyFolder_Valid(self):
        meta_info = {"info": {"BIDS": {"Folder": "", "extra": "test"}}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_ValidateMetaInfo_NonRequired_Valid(self):
        meta_info = {
            "info": {"BIDS": {"Run": "", "Ce": "", "Mod": "", "Acq": "", "Rec": ""}}
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_ValidateMetaInfo_RunValueIsInteger_Valid(self):
        meta_info = {"info": {"BIDS": {"Run": 1}}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")
        # Assert run numebr is still an integer
        self.assertEqual(meta_info["info"]["BIDS"]["Run"], 1)

    def test_ValidateMetaInfo_MissingTask_Invalid(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "template": "func_events_file",
                    "Filename": "example.tsv",
                    "Task": "",
                    "Modality": "bold",
                }
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == False
        self.assertFalse(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is correct
        self.assertEqual(
            meta_info["info"]["BIDS"]["error_message"],
            "Task '' does not match '^[a-zA-Z0-9]+$'",
        )

    def test_ValidateMetaInfo_MissingFilename_Invalid(self):
        meta_info = {
            "info": {
                "BIDS": {"template": "anat_file", "Filename": "", "extra": "test"},
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == False
        self.assertFalse(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is correct
        self.assertEqual(
            meta_info["info"]["BIDS"]["error_message"],
            "Filename '' is too short\n'Suffix' is a required property",
        )

    def test_ValidateMetaInfo_MissingModality_Invalid(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "template": "anat_file",
                    "Filename": "example.nii.gz",
                    "Suffix": "",
                }
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == False
        self.assertFalse(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is correct
        self.assertEqual(
            meta_info["info"]["BIDS"]["error_message"],
            "Suffix '' is not one of ['T1w', 'T2w', 'T1rho', 'T1map', 'T2map', 'T2star', 'FLAIR', 'FLASH', 'PD', 'PDmap', 'PDT2', 'inplaneT1', 'inplaneT2', 'angio', 'defacemask']",
        )

    def test_ValidateMetaInfo_NoTemplateMatch_Invalid(self):
        meta_info = {"info": {"BIDS": {"template": "NO_MATCH", "Modality": "bold"}}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == False
        self.assertFalse(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is correct
        self.assertEqual(
            meta_info["info"]["BIDS"]["error_message"], "Unknown template: NO_MATCH. "
        )

    def test_ValidateMetaInfo_NoEchoMatch_Invalid(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "template": "func_file",
                    "Filename": "example.nii.gz",
                    "Modality": "sbref",
                    "Task": "task",
                    "Rec": "",
                    "Run": "01",
                    "Echo": "AA",
                }
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == False
        self.assertFalse(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is correct
        self.assertEqual(
            meta_info["info"]["BIDS"]["error_message"],
            "Echo 'AA' does not match '^[0-9]*$'\n'Suffix' is a required property",
        )

    def test_ValidateMetaInfo_NoPropertyDefinitionMatch_Invalid(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "template": "anat_file",
                    "Modality": "invalid._#$*%",
                    "Ce": "invalid2.",
                    "Mod": "_invalid2",
                    "Filename": "",
                }
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == False
        self.assertFalse(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is correct
        self.assertEqual(
            meta_info["info"]["BIDS"]["error_message"],
            "Filename '' is too short\n'Suffix' is a required property\nMod '_invalid2' does not match '^[a-zA-Z0-9]*$'\nCe 'invalid2.' does not match '^[a-zA-Z0-9]*$'",
        )

    def test_ValidateMetaInfo_InfoDefinedNoBids_BidsIsNa(self):
        meta_info = {"info": {"test1": "abc", "test2": "def"}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS': 'NA' is in meta_info
        self.assertEqual(meta_info["info"]["BIDS"], "NA")

    def test_ValidateMetaInfo_InfoDefinedYesBids_BidsIsNa(self):
        meta_info = {"info": {"BIDS": "NA"}}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS': 'NA' is in meta_info
        self.assertEqual(meta_info["info"]["BIDS"], "NA")

    def test_ValidateMetaInfo_NoInfoDefined_BidsIsNa(self):
        meta_info = {"other_info": "test"}
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS': 'NA' is in meta_info
        self.assertEqual(meta_info["info"]["BIDS"], "NA")

    def test_ValidateMetaInfo_InfoDefined_BidsIsValid(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "Filename": "sub-01_ses-02_T1w.nii.gz",
                    "Modality": "T1w",
                    "valid": True,
                    "error_message": "",
                }
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_ValidateMetaInfo_InfoDefined_BidsIsInvalid(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "Task": "testtask",
                    "Modality": "bold",
                    "Filename": "sub-01_ses-02_task-testtask_bold.nii.gz",
                    "valid": False,
                    "error_message": "Missing required property: Task. ",
                }
            }
        }
        curate_bids.validate_meta_info(meta_info, DEFAULT_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_ValidateMetaInfo_InfoDefined_BIDSv0Template(self):
        meta_info = {
            "info": {
                "BIDS": {
                    "Task": "testtask",
                    "Modality": "bold",
                    "Filename": "sub-01_ses-02_task-testtask_bold.nii.gz",
                    "valid": False,
                    "error_message": "Missing required property: Task. ",
                }
            }
        }
        curate_bids.validate_meta_info(meta_info, BIDS_V1_TEMPLATE)
        # Assert 'BIDS.valid' == True
        self.assertTrue(meta_info["info"]["BIDS"]["valid"])
        # Assert error message is empty string
        self.assertEqual(meta_info["info"]["BIDS"]["error_message"], "")

    def test_intended_for(self):
        project = project_tree.TreeNode("project", {"label": "testProj"})

        template = curate_bids.set_up_template(None, {}, None, None)

        count = curate_bids.Count()

        count = curate_bids.curate_bids_tree(
            None, template, project, count, False, False
        )

        self.assertEqual(count.containers, 1)
        self.assertEqual(count.files, 0)
        self.assertEqual(count.acquisitions, 0)
        self.assertEqual(count.sessions, 0)

        session = project_tree.TreeNode(
            "session", {"label": "session1", "subject": {"code": "subj1"}}
        )
        project.children.append(session)

        acq1 = project_tree.TreeNode(
            "acquisition",
            {
                "label": "fmap-epi_acq-1",
                "created": "2018-01-17T07:58:09.799Z",
                "id": "shmy_d",
            },
        )
        session.children.append(acq1)
        file1 = project_tree.TreeNode(
            "file",
            {
                "name": "fieldmap.nii.gz",
                "type": "nifti",
                "classification": {"Intent": "Fieldmap"},
            },
        )
        acq1.children.append(file1)

        acq2 = project_tree.TreeNode(
            "acquisition",
            {"label": "func-bold_task-rest_run-1", "id": "no_stinking_badges"},
        )
        session.children.append(acq2)
        file2 = project_tree.TreeNode(
            "file",
            {
                "name": "task1.nii.gz",
                "type": "nifti",
                "modality": "MR",
                "classification": {"Intent": "Functional"},
            },
        )
        acq2.children.append(file2)

        count = curate_bids.curate_bids_tree(
            None, template, project, count, False, True
        )

        self.assertIn("IntendedFor", file1["info"])
        self.assertEqual(len(file1["info"]["IntendedFor"]), 1)
        self.assertEqual(
            file1["info"]["IntendedFor"][0],
            "ses-session1/func/sub-subj1_ses-session1_task-rest_run-1_bold.nii.gz",
        )
        self.assertEqual(
            count.containers, 4
        )  # = 1 project + 1 session + 2 acquisitions
        self.assertEqual(count.files, 2)
        self.assertEqual(count.acquisitions, 2)
        self.assertEqual(count.sessions, 1)


if __name__ == "__main__":

    unittest.main()
